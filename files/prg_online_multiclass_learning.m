%% main function
function prg_online_multiclass_learning()
clc,close all,clear all

% messages
fun_messages('Online Multi-class Learning','presentation');
fun_messages('Online Multi-class Learning','title');

% delete variables
fun_delete_variables();

% Random colors: This function computes a set of random colors that are
% used for visuzalization.
colors = fun_random_colors();

% Random means: This function computes random class means within the
% feature space.
means = fun_random_means();

% Samples: This function generates samples at random in the feature space.
[trnSamples,trnLabels] = fun_random_samples(means,'train');  % training samples
[tstSamples,tstLabels] = fun_random_samples(means,'test');  % test samples

% Show samples: This shows the samples in the feature space. Only two
% feature dimensions (x1,x2) are plotted.
fun_show_samples(trnSamples,trnLabels,means,colors,'train');  % training samples
fun_show_samples(tstSamples,tstLabels,means,colors,'test');  % test samples

% Classifiers: This function computes the set of class-specific classifiers.
% In this 2D classification problem, all classifiers are computed using the 
% same random ferns parameters.
clfrs = fun_classifiers();

% Online learning: This function trains the classifiers using the
% input samples that are given sequentially to the classifiers.
[clfrs,trnRes] = fun_learning(clfrs,trnSamples,trnLabels);

% Show learning results: This function shows the online learning results
% over the training samples.
fun_show_learning_results(trnRes,trnSamples,colors);

% Test: This function tests the computed classifiers on the test samples.
tstRes = fun_test(clfrs,tstSamples,tstLabels,colors);  % test samples

% save training and classification results
results.train = trnRes;  % training results -online learning-
results.test = tstRes;  % test results -generalization-
fun_data_save(results,'./variables/','results.mat');

% Save results: This functions saves the classifier and the classification
% results.
fun_save_results();

% message
fun_messages('End','title');
end
