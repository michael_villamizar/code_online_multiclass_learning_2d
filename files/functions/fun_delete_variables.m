%% Delete variables
% This function deletes the temporal variables.
function fun_delete_variables()
if (nargin~=0), fun_messages('Incorrect input parameters','error'); end

% delete specific variables
%delete('./variables/samples.mat');
%delete('./variables/labels.mat');
%delete('./variables/colors.mat');
%delete('./variables/means.mat');
%delete('./variables/classifiers.mat');

% delete all variables
delete('./variables/*.mat');

end
