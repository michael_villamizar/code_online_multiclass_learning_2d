%% Show learning results
% This function shows the online learning results over the training
% samples.
function fun_show_learning_results(results,samples,colors)
if (nargin~=3), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
fts = prms.visualization.fontSize;  % font size
mks = prms.visualization.markerSize;  % marker size
lnw = prms.visualization.lineWidth;  % line width
numClasses = prms.samples.numClasses;  % num. classes

% learning results
lbl = results.samples(:,1);  % true sample labels
est = results.samples(:,2);  % estimated sample labels -predictions-
typ = results.samples(:,3);  % training sample type
scr = results.samples(:,4);  % classification scores
rec = results.rates(:,1);  % recall rate
pre = results.rates(:,2);  % precision rate
fme = results.rates(:,3);  % f-measure rate
tps = results.rates(:,4);  % true positives rate
fps = results.rates(:,5);  % false positives rate
fns = results.rates(:,6);  % false negatives rate
rho = results.rates(:,7);  % classification accuracy
lambda = results.lambda;  % incremental classifier performance rate
nu = results.nu;  % num. human-labeled samples
mu = results.mu;  % num. machine-labeled samples
cfm = results.cfm;  %  confusion matrix
theta = results.theta;  % uncertainty region threshold
numPosSamples = results.numPosSamples;  % num. positive samples
numNegSamples = results.numNegSamples;  % num. negative samples

% num. samples
numSamples = size(rec,1);

% Samples scores: this computes the sample scores
% for the positive and negative classes and the 
% number of samples per class.
posScores = zeros(1,numClasses);
negScores = zeros(1,numClasses);
for iter = 1:numClasses
    % indexes
    posIndxs = find(lbl==iter);
    negIndxs = find(lbl==-1*iter);
    % scores
    posScores(1,iter) = sum(scr(posIndxs),1);
    negScores(1,iter) = sum(scr(negIndxs),1);
end

% average class scores
avePosScr = posScores./numPosSamples;  % positive scores
aveNegScr = negScores./numNegSamples;  % negative scores

% figure
figure,
subplot(231),plot(rec,'b-','linewidth',lnw,'markersize',mks),hold on;
plot(pre,'r--','linewidth',lnw,'markersize',mks),axis([1,numSamples,0,1]);
plot(fme,'k-','linewidth',lnw,'markersize',mks);
xlabel('# Samples','fontsize',fts),ylabel('Rates','fontsize',fts),grid on;
title('Recall-Precision','fontsize',fts),legend('Recall','Precision','F-measure');
subplot(232),imagesc(cfm),colormap(gray),hold on;
xlabel('Est. Class','fontsize',fts),ylabel('True Class','fontsize',fts);
title(sprintf('Accuracy: %.3f',rho(end)),'fontsize',fts);
subplot(233),plot(sum(nu,2),'b-','linewidth',lnw,'markersize',mks),hold on;
plot(sum(mu,2),'r-','linewidth',lnw,'markersize',mks),axis([1,numSamples,0,numSamples]);
legend('Human','Machine')
xlabel('# Samples','fontsize',fts),ylabel('# Annotations','fontsize',fts);
title('Sample Annotations','fontsize',fts),grid on;
subplot(234),bar(avePosScr),axis([0.5,numClasses+0.5,0,1]),hold on;
subplot(234),bar(avePosScr),axis([0.5,numClasses+0.5,0,1]),hold on;
xlabel('Classes','fontsize',fts),ylabel('Score','fontsize',fts);
title('Ave. Classification Scores','fontsize',fts),grid on;
subplot(235),plot(theta,'k-','linewidth',lnw,'markersize',mks), hold on;
axis([1,numSamples,0,1]), xlabel('# Samples','fontsize',fts);
ylabel('Theta','fontsize',fts);
title('Uncertainty Threshold','fontsize',fts),grid on;
subplot(236),plot(lambda,'m--','linewidth',lnw,'markersize',mks);
axis([1,numSamples,0,1]), xlabel('# Samples','fontsize',fts);
ylabel('Lambda','fontsize',fts);
title('Classifier Performance','fontsize',fts),grid on;

end
