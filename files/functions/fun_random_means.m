%% Random means
% This function computes random class means at random within the feature
% space. The class means are only computed for the positive classes.
function output = fun_random_means()
if (nargin~=0), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
numDims = prms.samples.numDims;  % num. feature space dimensions
distThr = prms.samples.distThr;  % class cluster distance threshold
spaceSize = prms.samples.spaceSize;  % feature space size
numClasses = prms.samples.numClasses;  % num. classes

% messages
fun_messages('random means','process');
fun_messages(sprintf('num. means: %d',numClasses),'information');

% load/compute random means
try
    % load previous random means
    means = fun_data_load('./variables/','means.mat');
catch ME

    % allocate
    means = zeros(numClasses,numDims);
    
    % random class means
    for iterMean = 1:numClasses
        % distance variable
        dist = 0;
        % check distance to previous class means
        while (dist<distThr)
            % random mean
            mean = spaceSize*rand(1,numDims);
            % distance metric
            for iter = 1:iterMean
                dist = sum(abs(mean - means(iter,:)),2);
                % check
                if (dist<distThr), break; end
            end
            % for the first mean
            if (iterMean==1),dist = 1; end
        end
        % class means
        means(iterMean,:) = mean;
    end
    
    % save random means
    fun_data_save(means,'./variables/','means.mat');
end

% output
output = means;
end
