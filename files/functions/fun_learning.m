%% Online learning:
% This function performs online learning of the classifiers using
% the input samples and their corresponding labels.
function [output1,output2] = fun_learning(clfrs,samples,labels)
if (nargin~=3), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % parameters
xi = prms.learning.xi;  % adaptive uncertainty region factor (xi)
tau = prms.learning.tau;  % incremental rate batch size
beta = prms.classifier.beta;  % classifier threshold (beta)
gamma = prms.learning.gamma;  % min. num. samples
numClasses = prms.samples.numClasses;  % num. classes (K)

% variables
tps = 0;  % true positives
fps = 0;  % false positives
fns = 0;  % false negatives
cfm = zeros(numClasses,numClasses);  % confusion matrix
numSamples = size(samples,1);  % num. samples

% variables
nu = zeros(1,numClasses);  % num. labeled samples -human annotation-
mu = zeros(1,numClasses);  % num. unlabeled samples -machine annotation- (not used)
batch = zeros(tau,numClasses);  % temporal rate batch
theta = ones(1,numClasses);  % uncertainty region thresholds (theta)
lambda = zeros(1,numClasses);  % incremental classification rate (lambda)
numCorr = zeros(1,numClasses);  % incremental number of correctly classified samples (M^c_k)
numAsst = zeros(1,numClasses);  % incremental number of human assistances (M^q_k)
numPosSamples = zeros(1,numClasses);  % num. positive samples
numNegSamples = zeros(1,numClasses);  % num. negative samples

% allocate learning results
data.nu = zeros(numSamples,numClasses);  % num. human annotations
data.mu = zeros(numSamples,numClasses);  % num. machine annotations (not used)
data.theta = zeros(numSamples,numClasses);  % uncertainty thresholds
data.rates = zeros(numSamples,7);  % learning rates
data.lambda = zeros(numSamples,numClasses);  % classifier performance
data.samples = zeros(numSamples,4);  % samples data

% messages
fun_messages('online learning:','process');
fun_messages(sprintf('num. samples: %d',numSamples),'information');

% online learning
tic;
for iterSample = 1:numSamples
    
    % Label type: this indicicates if the sample is used to update the
    % classifier or not (0), if the sample is labeled used the human
    % intervention -human annotation- (1) or the output of the classifier
    % -machine annotation- (2).
    type = 0;  % type {0,1,2}

    % current sample and its class label
    sample = samples(iterSample,:); % sample (x)
    label = labels(iterSample,1);  % sample label (y) {+,-}
    class = abs(label);  % sample class: class index {1,..,K}
   
    % update num. positive and negative samples
    if (label>0), numPosSamples(class) = numPosSamples(class) + 1; end
    if (label<0), numNegSamples(class) = numNegSamples(class) + 1; end
    %-------------------------------------------------
       
    
    
    %-------------- test classifiers -----------------
    % This tests all the classifiers in the current sample (x) 
    % to obtain the confidence of the classifiers.
    conf = fun_classifiers_test(clfrs,sample);
    
    % sample class estimation using the max. classifier output
    [maxConf,estLabel] = max(conf);  % max. confidence and estimated label
    if (maxConf<=beta), estLabel = -1*estLabel; end  % negative label
    estClass = abs(estLabel);  % estimated class  
    %-------------------------------------------------
    
    
    
    %------------ clasification rates ----------------
    % update classification rates
    if (label>0 && estLabel>0), tps = tps + 1; end  % true positives
    if (label<0 && estLabel>0), fps = fps + 1; end  % false positives
    if (label>0 && estLabel<0), fns = fns + 1; end  % false negatives
    
    % classification rates
    rec = tps/(tps+fns);  % recall
    pre = tps/(tps+fps);  % precision
    fme = 2*rec*pre/(rec+pre);  % f-measure
    
    % update confusion matrix
    if (label>0 && estLabel>0), cfm(label,estLabel) = cfm(label,estLabel) + 1; end
    
    % classification rate between classes (rho)
    tmp = (cfm+eps)./repmat(sum((cfm+eps),2),[1,numClasses]);  % normaliz.
    rho = sum(diag(tmp))/numClasses;  %  average diagonal rate
    %-------------------------------------------------
    
        
        
                
    % --------------- proposed learning --------------
    % The classifiers are computed using active learning in combination
    % with an adaptive uncertainty thresholds to reduce the degree of
    % human assistance as the confidence of the classifiers gets larger.
                
    % human assistance request q(x)
    q = ((maxConf < beta + 0.5*theta(class)) && (maxConf > beta - 0.5*theta(class)));        
                
    % Active learning: The classifiers are computed with uncertain samples.
    % Additionally, the classifiers are initialized with a minimum number of 
    % samples (gamma).
    if (q==1 || iterSample<=gamma)
                    
        % sample type
        type = 1;
                    
        % update num. human annotations
        nu(1,class) = nu(1,class) + 1;
                 
        % update num. human assistances (M^q_k)
        numAsst(class) = numAsst(class) + 1;
        
        % Incremental classifier performance rate (lambda). This measures
        % the performance of a classifier before knowing the true class 
        % label given by the human and before updating the classifier. 
        % This performance rate can only be computed when the human label 
        % is available since the estimated class label (estLabel) is compared 
        % against the true label (human label).
        if ((label>0) && (class==estClass)), numCorr(class) = numCorr(class) + 1; end
        if ((label<0) && (estLabel<0)), numCorr(class) = numCorr(class) + 1; end
        lambda(class) = numCorr(class)/numAsst(class);

        % update the classifiers using the input sample and the
        % human annotation -label-.
        clfrs = fun_classifiers_update(clfrs,sample,label);
                  
        % update classifier in cases of misclassification
        if ((label>0) && (class~=estClass)), clfrs = fun_classifiers_update(clfrs,sample,-1*estClass); end
        if ((label<0) && (estLabel>0)), clfrs = fun_classifiers_update(clfrs,sample,-1*estClass); end
   
        %---------- adaptive uncertainty region ----------
        % incremental classifier rate batch
        batch = [batch(2:end,:);lambda];
        
        % adaptative human-labeling interval (uncertainty region) according
        % to the classifier performance (using the batch)
        theta = max(0,1 - xi*mean(batch,1));
                
    end
   
    % learning data:
    data.nu(iterSample,:) = nu;  % num. human labels
    data.mu(iterSample,:) = mu;  % num. machine labels (not used)
    data.lambda(iterSample,:) = lambda;  % classifier performance rates
    data.theta(iterSample,:) = theta;  % uncertainty thresholds
    data.rates(iterSample,:) = [rec,pre,fme,tps,fps,fns,rho];  % classification rates
    data.samples(iterSample,:) = [label,estLabel,type,maxConf];  % sample information
    
    % message
    if(mod(iterSample,100)==0), fun_messages(sprintf('sample: %d/%d',...
            iterSample,numSamples),'information'); end
end

% learning data
data.cfm = cfm;  % final confusion matrix
data.numPosSamples = numPosSamples;  % num. positive samples
data.numNegSamples = numNegSamples;  % num. negative samples

% output
output1 = clfrs;  % classifiers
output2 = data;  % learning data
end
