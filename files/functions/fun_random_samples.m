%% Samples
% This function generates positive and negative samples at random in the
% feature space. Positive samples are computed according to the input class
% means, whereas the negative ones are computed at random over the whole
% feature space. This function returns the samples and their corresponding
% labels.
function [output1,output2] = fun_random_samples(means,txt)
if (nargin~=2), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
distThr = prms.samples.distThr;  % class distance threshold
numDims = prms.samples.numDims;  % num. feature space dimensions
classStd = prms.samples.classStd;  % class standard deviation
spaceSize = prms.samples.spaceSize;  % feature space size
numClasses = prms.samples.numClasses;  % num. positive classes
numPosSamples = prms.samples.numPosSamples;  % num. positive class samples
numNegSamples = prms.samples.numNegSamples;  % num. negative class samples

% message
fun_messages(sprintf('%s samples',txt),'process');
fun_messages(sprintf('num. classes: %d',numClasses),'information');
fun_messages(sprintf('num. positive samples: %d',numPosSamples),'information');
fun_messages(sprintf('num. negative samples: %d',numNegSamples),'information');

% load/compute class samples
try
    
    % load previous samples and labels
    samples = fun_data_load('./variables/',sprintf('%s_samples.mat',txt));
    labels = fun_data_load('./variables/',sprintf('%s_labels.mat',txt));
    
catch ME
    
    % samples class indexes -positives-
    indxs = max(1,min(numClasses,ceil(numClasses*rand(numPosSamples,1))));
    
    % allocate
    posLabels = zeros(numPosSamples,1);  % positive samples labels -class index-
    posSamples = zeros(numPosSamples,numDims);  % positive samples data -points-
    negLabels = zeros(numNegSamples,1);  % negative samples labels -class index-
    negSamples = zeros(numNegSamples,numDims);  % negative samples data -points-
    
    % positive samples
    for iterSample = 1:numPosSamples
        % class label
        label = indxs(iterSample);
        % class sample
        posSamples(iterSample,:) = means(label,:) + classStd.*randn(1,numDims);
        % class labels: [1,numClasses]
        posLabels(iterSample,1) = label;
    end
    
    % negative samples
    for iterSample = 1:numNegSamples
        % distance variable
        dist = 0;
        % check distance to positive class means
        while (dist<distThr)
            % random sample
            sample = spaceSize*rand(1,numDims);
            % distance metric
            for iterMean = 1:size(means,1)
                dist = sum(abs(sample - means(iterMean,:)),2);
                % check
                if (dist<distThr), break; end
            end
        end
        % class sample
        negSamples(iterSample,:) = sample;
        % class labels: [-1,-numClasses]
        label = max(1,min(numClasses,round(rand*numClasses)));
        negLabels(iterSample,1) = -1*label;
    end
    
    % samples and labels
    samples = [posSamples;negSamples];
    labels = [posLabels;negLabels];
    
    % random order
    indxs = randperm(size(samples,1));
    samples = samples(indxs,:);
    labels = labels(indxs,:);
    
    % save samples and labels
    fun_data_save(samples,'./variables/',sprintf('%s_samples.mat',txt));
    fun_data_save(labels,'./variables/',sprintf('%s_labels.mat',txt));
    
end

% output
output1 = samples;  % samples data
output2 = labels;  % samples labels
end
