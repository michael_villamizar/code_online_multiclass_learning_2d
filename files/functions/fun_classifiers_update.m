%% Classifiers update
% This function updates the classifiers using the input sample 
% and its label.
function output = fun_classifiers_update(clfrs,sample,label)
if (nargin~=3), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % parameters
numFerns = prms.classifier.numFerns;  % num. random ferns (J)
numFeats = prms.classifier.numFeats;  % num. binary features (M) 

% sample class
class = abs(label);

% allocate
fernOuts = zeros(1,numFerns);  % fern outputs

% Feature computation: This tests the set of shared ferns (Omega) 
% in advance in order to keep efficiency. All classifiers share 
% the same set of ferns.
for r = 1:numFerns
    
    % fern output
    z = 1;

    % binary features
    for m = 1:numFeats
        
        % feature (x_i) and threshold (phi)
        x_i = clfrs.ferns(r,m,1);
        phi = clfrs.ferns(r,m,2);
        
        % sample value
        value = sample(1,x_i);
        
        % update output
        if (value>phi), z = z + 2^(m-1); end
        
    end
    
    % fern outputs
    fernOuts(1,r) = z;
    
end

% update class distributions
for j = 1:numFerns
    
    % fern output
    z = fernOuts(j);
    
    % update class distributions
    if (label>0), clfrs.clfr(class).posHstms(j,z) = clfrs.clfr(class).posHstms(j,z) + 1; end % N^(j,z)_(k,+1)
    if (label<0), clfrs.clfr(class).negHstms(j,z) = clfrs.clfr(class).negHstms(j,z) + 1; end % N^(j,z)_(k,-1)
    
    % update ratio class distributions
    p = clfrs.clfr(class).posHstms(j,z);  % N^(j,z)_(k,+1)
    n = clfrs.clfr(class).negHstms(j,z);  % N^(j,z)_(k,-1)
    clfrs.clfr(class).ratHstms(j,z) = p/(p+n);  % eta^(k,z)_k
    
end

% output
output = clfrs;
end
