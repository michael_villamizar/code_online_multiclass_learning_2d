%% Show samples
% This function shows the samples in the feature space. Only two feature
% dimension of the samples are shown.
function fun_show_samples(samples,labels,means,colors,txt)
if (nargin~=5), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
fts = prms.visualization.fontSize;  % font size
mks = prms.visualization.markerSize;  % marker size
lnw = prms.visualization.lineWidth;  % line width
numMeans = size(means,1);  % num. class means
spaceSize = prms.samples.spaceSize;  % feature space size
numSamples = size(samples,1);  % num. samples

% show samples -only two first dimensions-
figure,grid on, axis([0,spaceSize,0,spaceSize]),hold on;
for iter = 1:numSamples
    % sample properties
    x1 = samples(iter,1);  % dim. x1
    x2 = samples(iter,2);  % dim. x2
    label = labels(iter,:);  % class label
    % plot sample
    if (label>0),plot(x1,x2,'.','color',colors(label,:),'markersize',...
            mks,'linewidth',lnw),hold on; end
    if (label<0),plot(x1,x2,'.','color','k','markersize',mks,...
            'linewidth',lnw),hold on; end
end
xlabel('x1','fontsize',fts),ylabel('x2','fontsize',fts);
title(sprintf('%s samples',txt),'fontsize',fts);

% show class means
for iter = 1:numMeans
    % means properties
    x1 = means(iter,1);  % dim. x1
    x2 = means(iter,2);  % dim. x2
    % plot sample
    text(x1,x2,sprintf('%d',iter)),hold on;
end

end
