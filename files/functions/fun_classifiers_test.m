%% Classifiers test
% This function tests the classifiers in the input sample.
function output = fun_classifiers_test(clfrs,sample)
if (nargin~=2), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % parameters
numFerns = prms.classifier.numFerns;  % num. ferns (J)
numFeats = prms.classifier.numFeats;  % num. features (M)
numClasses = prms.samples.numClasses;  % num. classes (K)

% allocate
conf = zeros(1,numClasses);  % classifier confidence
fernOuts = zeros(1,numFerns);  % fern outputs

% Feature computation: This tests the set of shared random
% ferns (Omega) in advance in order to keep efficiency. All 
% classifiers share the same set of ferns.
for r = 1:numFerns
    
    % fern output
    z = 1;

    % binary features
    for m = 1:numFeats
        
        % feature (x_i) and threshold (phi)
        x_i = clfrs.ferns(r,m,1);
        phi = clfrs.ferns(r,m,2);
        
        % sample value
        value = sample(1,x_i);
        
        % update output
        if (value>phi), z = z + 2^(m-1); end
        
    end
    
    % fern outputs
    fernOuts(1,r) = z;
    
end

% Classifiers test: This tests all classifiers using 
% the previous computed fern outputs.
for j = 1:numFerns
    
    % fern output
    z = fernOuts(j);

    % classes
    for c = 1:numClasses
        
        % update classifier confidence
        conf(c) = conf(c) + clfrs.clfr(c).ratHstms(j,z);
        
    end
end

% normalization
conf = conf./numFerns;

% output
output = conf;
end
