%% Parameters
% This function contains the program parameters.
function output = fun_parameters()
if (nargin~=0), fun_messages('Incorrect input parameters','error'); end

% experiment
tag = 'exp1';  % experiment name or tag
experiment = struct('tag',tag);

% samples
numDims = 2;  % num. feature space dimensions
distThr = 0.13;  % class distance threshold
classStd = 0.025;  % class standard deviation
spaceSize = 1.00;  % feature space size
numClasses = 10;  % num. positive classes (K)
numPosSamples = 1500;  % num. positive class samples
numNegSamples = 1500;  % num. negative class samples
samples = struct('numClasses',numClasses,'numPosSamples',numPosSamples,...
    'numNegSamples',numNegSamples,'classStd',classStd,'spaceSize',...
    spaceSize,'numDims',numDims,'distThr',distThr);

% classifier: online random ferns
beta = 0.50;  % classfier threshold (beta)
numFeats = 7;  % num. features (M)
numFerns = 100;  % num. random ferns (J)
classifier = struct('beta',beta,'numFerns',numFerns,'numFeats',numFeats);

% learning
xi = 1.1;  % adaptive uncertainty region factor (xi)
tau = 10;  % incremental rate batch size
gamma = 200;  % min. num. samples
learning = struct('gamma',gamma,'xi',xi,'tau',tau);

% visualization
gridSize = 100;  % spatial grid size
fontSize = 14;  % font size
lineWidth = 4;  % line width
markerSize = 25;  % marker size
visualization = struct('markerSize',markerSize,'lineWidth',lineWidth,'fontSize',...
    fontSize,'gridSize',gridSize);

% parameters
prms.samples = samples;
prms.learning = learning;
prms.classifier = classifier;
prms.experiment = experiment;
prms.visualization = visualization;

% output
output = prms;
end
