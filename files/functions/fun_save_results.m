%% Save results
% This function saves the classification results and the temporal variables
% at specific folder.
function fun_save_results()
if (nargin~=0), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
tag = prms.experiment.tag;  % experiment tag

% output directory
mkdir(sprintf('./results/%s',tag));

% copy variables
copyfile('./variables/*.mat',sprintf('./results/%s/',tag));

end
