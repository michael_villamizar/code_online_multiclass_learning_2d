% Classifiers 
% This function computes the set of class-specific classifiers.
% Each classifier is an online random ferns classifier using 
% decision stumps in the 2D feature space. To reduce the computational
% cost, all classifiers share the same ferns parameters.
function output = fun_classifiers()
if (nargin~=0), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % parameters
numFerns = prms.classifier.numFerns;  % num. random ferns (J)
numFeats = prms.classifier.numFeats;  % num. binary features (M)
numClasses = prms.samples.numClasses;  % num. positive classes (K)

% message
fun_messages('classifiers','process');
fun_messages(sprintf('num. ferns: %d',numFerns),'information');
fun_messages(sprintf('num. features: %d',numFeats),'information');
fun_messages(sprintf('num. classes: %d',numClasses),'information');

% load/compute the classifiers
try

    % load previous classifiers
    clfrs = fun_data_load('./variables/','classifiers.mat');

catch ME
    
    % Shared random ferns (Omega): This function builds a set of shared 
    % random ferns. They are used to compute each classifier (H_k). 
    clfrs.ferns = fun_ferns();
    
    % classifiers
    for k = 1:numClasses

        % mesages
        fun_messages(sprintf('computing classifier: %d/%d',k,numClasses),'information');

        % Classifier (H_k): This initializes a class-specific classifier with empty values. 
        % The class probabilities are set to ones.
        clfrs.clfr(k) = fun_classifier();
    end
    
    % save classifiers 
    fun_data_save(clfrs,'./variables/','classifiers.mat');

end

% output
output = clfrs;
end

%% Classifier: online random ferns (orfs)
% This function computes an online random ferns classifier (H_k).
function output = fun_classifier()
if (nargin~=0), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % parameters
numFerns = prms.classifier.numFerns;  % num. random ferns (J)
numFeats = prms.classifier.numFeats;  % num. binary features (M)

% allocate
posHstms = ones(numFerns,2^numFeats);  % positive class distributions (N^(j,z)_(k,+1))
negHstms = ones(numFerns,2^numFeats);  % negative class distributions (N^(j,z)_(k,-1))
ratHstms = 0.5*ones(numFerns,2^numFeats);  % ratio class distributions (eta^(j,z)_k)

% classifier (H_k)
clfr.posHstms = posHstms;  % positive class distributions
clfr.negHstms = negHstms;  % negative class distributions
clfr.ratHstms = ratHstms;  % ratio class distributions

% output
output = clfr;
end

% Shared random ferns (Omega)
% This function builds the set of shared random ferns (Omega) which are used 
% later to compute each classifier (H_k). This reduces the computational cost
% since all classifiers share the same ferns features. However, in this 2D 
% classification problem, the number of shared ferns parameters (omega) 
% corresponds to the number of random ferns (R = J) since there is not spatial 
% information like in the case of images.
function output = fun_ferns()
if (nargin~=0), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % parameters
numDims = prms.samples.numDims;  % num. feature space dimensions
numFerns = prms.classifier.numFerns;  % num. random ferns (J)
numFeats = prms.classifier.numFeats;  % num. binary features (M)

% allocate
ferns = zeros(numFerns,numFeats,2);

% Random ferns computation: Each fern feature is a decision stump 
% over the 2D feature space.
for r = 1:numFerns
    for m = 1:numFeats

        % feature: axis (x_i) and threshold (phi)
        x_i = min(numDims,ceil(rand*numDims));
        phi = rand;

        % save
        ferns(r,m,1) = x_i;
        ferns(r,m,2) = phi;
    end
end

% output
output = ferns;
end
