%% Test
% This function tests the online random ferns classifiers on 
% the test samples to measure the generalization capabilities.
function output = fun_test(clfrs,samples,labels,colors)
if (nargin~=4), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
fts = prms.visualization.fontSize;  % font size
mks = prms.visualization.markerSize;  % marker size
lnw = prms.visualization.lineWidth;  % line width
beta = prms.classifier.beta;  % classifier threshold (beta)
numSamples = size(samples,1);  % num. samples
numClasses = prms.samples.numClasses;  % num. classes (K)

% messages
fun_messages('test:','process');
fun_messages(sprintf('num. samples: %d',numSamples),'information');

% variables
tp = 0;  % true positives
fp = 0;  % false positives
fn = 0;  % false negatives
cfm = zeros(numClasses,numClasses);  % confusion matrix
estLabels = zeros(1,numSamples);  % estiated labels

% samples
tic;
for iterSample = 1:numSamples
    
    %-------------- current sample -------------------
    % current sample and its class label
    label = labels(iterSample,1);  % sample label (y)
    sample = samples(iterSample,:);  % sample data (x)
    %-------------------------------------------------
    
    
    %-------------- test classifier ------------------
    % test the classifiers in the current sample (x) to
    % obtain the classifier confidence.
    conf = fun_classifiers_test(clfrs,sample);
    
    % sample class estimation using the classifier output
    [conf,estLabel] = max(conf);  % max. confidence and estimated label
    if (conf<beta), estLabel = -1; end  % negative label
    estLabels(iterSample) = estLabel;  % estimated class
    %-------------------------------------------------
    
    
    %------------ clasification rates ----------------
    % update classification rates
    if (label>0 && estLabel>0), tp = tp + 1; end  % true positives
    if (label<0 && estLabel>0), fp = fp + 1; end  % false positives
    if (label>0 && estLabel<0), fn = fn + 1; end  % false negatives
    
    % update confusion matrix
    if (label>0 && estLabel>0), cfm(label,estLabel) = cfm(label,estLabel) + 1; end
    %-------------------------------------------------
    
    % message
    if(mod(iterSample,100)==0), fun_messages(sprintf('sample: %d/%d',...
            iterSample,numSamples),'information'); end
end

% classification rates
rec = tp/(tp+fn);  % recall
pre = tp/(tp+fp);  % precision
fme = 2*rec*pre/(rec+pre);  % f-measure

% confusion matrix: normalization
cfm = (cfm+eps)./repmat(sum((cfm+eps),2),[1,numClasses]);
rho =sum(diag(cfm))/numClasses;  % average diagonal rate

% message
fun_messages('test results','process');
fun_messages(sprintf('time: %.3f [sec.]',toc),'information');
fun_messages(sprintf('recall: %.3f',rec),'information');
fun_messages(sprintf('precision: %.3f',pre),'information');
fun_messages(sprintf('f-measure: %.3f',fme),'information');
fun_messages(sprintf('average diagonal rate: %.3f',rho),'information');

% figure
figure,subplot(121),bar([rec,pre,fme]);grid on;
xlabel('Rec|Pre|FMe','fontsize',fts),ylabel('Rate','fontsize',fts);
title('Test - Classification Rates','fontsize',fts);
subplot(122),imagesc(cfm),hold on;
xlabel('Est. Class','fontsize',fts),ylabel('True Class','fontsize',fts);
title(sprintf('Test - Accuracy: %.3f',rho),'fontsize',fts);

% show classification results
figure, axis([0,1,0,1]),hold on;
for iter = 1:numSamples

    % sample properties
    x1 = samples(iter,1);  % x1
    x2 = samples(iter,2);  % x2
    label = labels(iter);  % label
    estLabel = estLabels(iter);  % estimated label
    
    % true positives
    if (label>0 && estLabel>0)
        plot(x1,x2,'.','color',colors(label,:),'linewidth',lnw,'markersize',mks),hold on;
end
    % true negatives
    if (label<0 && estLabel<0)
        plot(x1,x2,'.','color','k','linewidth',lnw,'markersize',mks),hold on;
    end
    % false positives
    if (label<0 && estLabel>0)
        plot(x1,x2,'.','color','k','linewidth',lnw,'markersize',mks),hold on;
        plot(x1,x2,'o','color','r','linewidth',lnw,'markersize',mks),hold on;
    end
    % false negatives
    if (label>0 && estLabel<0)
        plot(x1,x2,'.','color',colors(label,:),'linewidth',lnw,'markersize',mks),hold on;
        plot(x1,x2,'o','color','r','linewidth',lnw,'markersize',mks),hold on;
    end
end
xlabel('x1','fontsize',fts),ylabel('x2','fontsize',fts);
title('Test Samples','fontsize',fts),grid on;

% classification results
results.rec = rec;  % recall
results.pre = pre;  % precision
results.fme = fme;  % f-measure
results.cfm = cfm;  % confusion matrix
results.rho = rho;  % average diagonal rate

% output
output = results;
end
