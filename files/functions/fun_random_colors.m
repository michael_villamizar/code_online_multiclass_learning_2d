%% Random colors
% This function computes a set of  random colors used for visuzalization.
function output = fun_random_colors()
if (nargin~=0), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
numClasses = prms.samples.numClasses;  % num. classes

% messages
fun_messages('random colors','process');
fun_messages(sprintf('num. colors: %d',numClasses),'information');

% load/compute random colors
try
    % load previous colors
    colors = fun_data_load('./variables/','colors.mat');
catch ME
    % random colors
    colors = rand(numClasses,3);
    % save colors
    fun_data_save(colors,'./variables/','colors.mat');
end

% output
output = colors;
end
