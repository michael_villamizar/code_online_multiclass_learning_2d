
 Online Multi-class Learning

 Description: 
   This code computes multiple online random ferns classifiers with small
   human supervision for a 2D multi-class problem. The proposed algorithm 
   uses active learning in combination with an adaptive uncertainty threshold 
   in order to reduce the degree of human assistance as the classifier confidence 
   gets larger. To keep efficiency, all classifiers are computed with the same 
   ferns features.

   For more detail refer to reference [1].

 Comments:
   The parameters of the classifiers and the 2D feature scenario can be found 
   in the fun_parameters.m function.

   In this code, the training and test samples are generated at random. The
   positive samples correspond to K different clusters whereas the negative
   samples are spread over the entire 2D feature space.

   If you make use of this code for research articles, we kindly encourage
   to cite the reference [1], listed below. This code is only for research 
   and educational purposes.

 Steps:
   Steps to exucute the program:
     1. Run the prg_setup.m file to configure the program paths.
     2. Run the prg_online_multiclass_learning.m file to compute the classifiers 
        and to perfom classification on the 2D problem scenario. 

 References:
   [1] Modeling Robot's World with Minimal Effort
       M. Villamizar, A. Garrell, A. Sanfeliu and F. Moreno-Noguer
       International Conference on Robotics and Automation (ICRA)
       Seattle, USA, July 2015 

 Contact:
   Michael Villamizar
   mvillami-at-iri.upc.edu
   Institut de Robòtica i Informática Industrial CSIC-UPC
   Barcelona - Spain
   2016
